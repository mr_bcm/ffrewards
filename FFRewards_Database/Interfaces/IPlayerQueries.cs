﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;

namespace FFRewards_Database.Interfaces
{
    public interface IPlayerQueries
    {
        Player GetPlayer(int playerId);

        List<Player> GetPlayers();

        List<Player> GetPlayers(Team team);

        List<Player> GetFreeAgents(League league);

        void AddPlayertoTeam(Team team, Player player);

        void DeletePlayer(Team team, Player player);

        void UpdatePlayer(Team team, Player player);

        //List<Player> GetPlayers(Team team);

        //List<Player> GetPlayers(ApplicationUser user, int teamId);



    }
}
