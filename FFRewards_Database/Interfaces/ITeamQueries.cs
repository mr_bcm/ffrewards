﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;

namespace FFRewards_Database.Interfaces
{
    public interface ITeamQueries
    {
        Team GetTeam(int teamId);

        Team GetTeam(ApplicationUser user, League league);

        List<Team> GetTeams(ApplicationUser user);

        List<Team> GetTeams(int leagueId);

        void AddNewTeam(ApplicationUser user, Team team);

        void UpdateTeam(ApplicationUser user, Team team);

        void DeleteTeam(ApplicationUser user, Team team);

    }
}