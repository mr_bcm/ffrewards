﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;

namespace FFRewards_Database.Interfaces
{
    public interface ILeagueQueries
    {
        League GetLeague(int leagueId);

        List<League> GetLeagues();

        List<League> GetLeagues(int leagueType);

        List<League> GetLeagues(ApplicationUser user);

        List<League> GetLeagues(Sponsor sponsor);

        void AddNewLeague(ApplicationUser user, Sponsor sponsor, League league, Rules rule);

        void DeleteLeague(ApplicationUser user, League league);

        void UpdateLeague(ApplicationUser user, League league);
    }
}
