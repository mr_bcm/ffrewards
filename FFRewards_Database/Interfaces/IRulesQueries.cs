﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;

namespace FFRewards_Database.Interfaces
{
    public interface IRulesQueries
    {
        Rules GetRules(int ruleId);

        List<Rules> GetRulesList(ApplicationUser user);

        void AddNewRule(ApplicationUser user, Rules rule);

        void UpdateRules(ApplicationUser user, Rules rule);

        void DeleteRules(ApplicationUser user, Rules rule);
    }
}
