﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;

namespace FFRewards_Database.Interfaces
{
    public interface ISponsorQueries
    {
        Sponsor GetSponsor(int sponsorId);

        List<Sponsor> GetSponsors();

        void AddNewSponsor(Sponsor sponsor);

        void DeleteSponsor(Sponsor sponsor);

        void UpdateSponsor(Sponsor sponsor);
    }
}
