﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;

namespace FFRewards_Database.Interfaces
{
    public interface IParticipantQueries
    {

        List<ApplicationUser> GetParticipants(League league);

        void AddParticipants(ApplicationUser user, League league);
    }
}
