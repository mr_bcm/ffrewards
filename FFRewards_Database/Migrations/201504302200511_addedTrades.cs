namespace FFRewards_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedTrades : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Trades",
                c => new
                    {
                        TradeId = c.Int(nullable: false, identity: true),
                        PlayerId = c.Int(nullable: false),
                        STeamId = c.Int(nullable: false),
                        DTeamId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TradeId)
                .ForeignKey("dbo.Teams", t => t.DTeamId, cascadeDelete: false)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.STeamId, cascadeDelete: false)
                .Index(t => t.PlayerId)
                .Index(t => t.STeamId)
                .Index(t => t.DTeamId);
            AddColumn("dbo.Players", "League_LeagueID", c => c.Int());
            CreateIndex("dbo.Players", "League_LeagueID");
            AddForeignKey("dbo.Players", "League_LeagueID", "dbo.Leagues", "LeagueID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trades", "STeamId", "dbo.Teams");
            DropForeignKey("dbo.Trades", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Trades", "DTeamId", "dbo.Teams");
            DropForeignKey("dbo.Players", "League_LeagueID", "dbo.Leagues");
            DropIndex("dbo.Trades", new[] { "DTeamId" });
            DropIndex("dbo.Trades", new[] { "STeamId" });
            DropIndex("dbo.Trades", new[] { "PlayerId" });
            DropIndex("dbo.Players", new[] { "League_LeagueID" });
            DropColumn("dbo.Players", "League_LeagueID");
            DropTable("dbo.Trades");
        }
    }
}
