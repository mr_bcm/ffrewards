namespace FFRewards_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userEmailDeleted : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "UserEmail");
            DropColumn("dbo.AspNetUsers", "UserPhoneNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "UserPhoneNumber", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserEmail", c => c.String());
        }
    }
}
