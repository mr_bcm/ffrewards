namespace FFRewards_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Leagues",
                c => new
                    {
                        LeagueID = c.Int(nullable: false, identity: true),
                        OwnerId = c.String(maxLength: 128),
                        RulesId = c.Int(nullable: false),
                        SponsorId = c.Int(nullable: false),
                        LeagueName = c.String(),
                        Prize = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.LeagueID)
                .ForeignKey("dbo.Rules", t => t.RulesId, cascadeDelete: true)
                .ForeignKey("dbo.Sponsors", t => t.SponsorId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId)
                .Index(t => t.OwnerId)
                .Index(t => t.RulesId)
                .Index(t => t.SponsorId);
            
            CreateTable(
                "dbo.Rules",
                c => new
                    {
                        RulesID = c.Int(nullable: false, identity: true),
                        RuleName = c.String(),
                        OwnerID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.RulesID)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerID)
                .Index(t => t.OwnerID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(maxLength: 256),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        Admin = c.Boolean(nullable: false),
                        ImageUrl = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        TeamId = c.Int(nullable: false, identity: true),
                        OwnerId = c.String(maxLength: 128),
                        LeagueId = c.Int(nullable: false),
                        TeamName = c.String(),
                    })
                .PrimaryKey(t => t.TeamId)
                .ForeignKey("dbo.Leagues", t => t.LeagueId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId)
                .Index(t => t.OwnerId)
                .Index(t => t.LeagueId);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        PlayerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        TeamName = c.String(),
                        Injured = c.Boolean(nullable: false),
                        Position = c.String(),
                        Active = c.Boolean(nullable: false),
                        PassCompleted = c.String(),
                        PassAttempted = c.String(),
                        PassYards = c.String(),
                        PassTouchdown = c.String(),
                        PassInterception = c.String(),
                        RushAttempts = c.String(),
                        RushYards = c.String(),
                        RushTouchdowns = c.String(),
                        ReceiveSuccess = c.String(),
                        ReceiveYards = c.String(),
                        ReceiveTouchdowns = c.String(),
                        ReceiveTarget = c.String(),
                        Total2PointCon = c.String(),
                        TotalFumbleLost = c.String(),
                        TotalRetTouchdown = c.String(),
                        Team_TeamId = c.Int(),
                    })
                .PrimaryKey(t => t.PlayerId)
                .ForeignKey("dbo.Teams", t => t.Team_TeamId)
                .Index(t => t.Team_TeamId);
            
            CreateTable(
                "dbo.Sponsors",
                c => new
                    {
                        SponsorID = c.Int(nullable: false, identity: true),
                        SponsorName = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        Payout = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LogoURL = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.SponsorID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Leagues", "OwnerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Leagues", "SponsorId", "dbo.Sponsors");
            DropForeignKey("dbo.Leagues", "RulesId", "dbo.Rules");
            DropForeignKey("dbo.Rules", "OwnerID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Teams", "OwnerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Players", "Team_TeamId", "dbo.Teams");
            DropForeignKey("dbo.Teams", "LeagueId", "dbo.Leagues");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Players", new[] { "Team_TeamId" });
            DropIndex("dbo.Teams", new[] { "LeagueId" });
            DropIndex("dbo.Teams", new[] { "OwnerId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Rules", new[] { "OwnerID" });
            DropIndex("dbo.Leagues", new[] { "SponsorId" });
            DropIndex("dbo.Leagues", new[] { "RulesId" });
            DropIndex("dbo.Leagues", new[] { "OwnerId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Sponsors");
            DropTable("dbo.Players");
            DropTable("dbo.Teams");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Rules");
            DropTable("dbo.Leagues");
        }
    }
}
