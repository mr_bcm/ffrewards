namespace FFRewards_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix : DbMigration
    {
        public override void Up()
        {

            CreateTable(
                "dbo.PlayerTeams",
                c => new
                    {
                        Player_PlayerId = c.Int(nullable: false),
                        Team_TeamId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Player_PlayerId, t.Team_TeamId })
                .ForeignKey("dbo.Players", t => t.Player_PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.Team_TeamId, cascadeDelete: true)
                .Index(t => t.Player_PlayerId)
                .Index(t => t.Team_TeamId);
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PlayerTeam1",
                c => new
                    {
                        Player_PlayerId = c.Int(nullable: false),
                        Team_TeamId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Player_PlayerId, t.Team_TeamId });
            
            CreateTable(
                "dbo.PlayerTeams",
                c => new
                    {
                        PlayerId = c.Int(nullable: false),
                        TeamId = c.Int(nullable: false),
                        Starter = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.PlayerId, t.TeamId });
            
            DropForeignKey("dbo.PlayerTeams", "Team_TeamId", "dbo.Teams");
            DropForeignKey("dbo.PlayerTeams", "Player_PlayerId", "dbo.Players");
            DropIndex("dbo.PlayerTeams", new[] { "Team_TeamId" });
            DropIndex("dbo.PlayerTeams", new[] { "Player_PlayerId" });
            DropTable("dbo.PlayerTeams");
            CreateIndex("dbo.PlayerTeam1", "Team_TeamId");
            CreateIndex("dbo.PlayerTeam1", "Player_PlayerId");
            CreateIndex("dbo.PlayerTeams", "PlayerId");
            AddForeignKey("dbo.PlayerTeams", "PlayerId", "dbo.Players", "PlayerId", cascadeDelete: true);
            AddForeignKey("dbo.PlayerTeam1", "Team_TeamId", "dbo.Teams", "TeamId", cascadeDelete: true);
            AddForeignKey("dbo.PlayerTeam1", "Player_PlayerId", "dbo.Players", "PlayerId", cascadeDelete: true);
        }
    }
}
