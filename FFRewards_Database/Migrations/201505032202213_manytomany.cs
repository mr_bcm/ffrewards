namespace FFRewards_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class manytomany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Players", "League_LeagueID", "dbo.Leagues");
            DropIndex("dbo.Players", new[] { "League_LeagueID" });
            CreateTable(
                "dbo.PlayerLeagues",
                c => new
                    {
                        Player_PlayerId = c.Int(nullable: false),
                        League_LeagueID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Player_PlayerId, t.League_LeagueID })
                .ForeignKey("dbo.Players", t => t.Player_PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Leagues", t => t.League_LeagueID, cascadeDelete: true)
                .Index(t => t.Player_PlayerId)
                .Index(t => t.League_LeagueID);
            
            DropColumn("dbo.Players", "League_LeagueID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Players", "League_LeagueID", c => c.Int());
            DropForeignKey("dbo.PlayerLeagues", "League_LeagueID", "dbo.Leagues");
            DropForeignKey("dbo.PlayerLeagues", "Player_PlayerId", "dbo.Players");
            DropIndex("dbo.PlayerLeagues", new[] { "League_LeagueID" });
            DropIndex("dbo.PlayerLeagues", new[] { "Player_PlayerId" });
            DropTable("dbo.PlayerLeagues");
            CreateIndex("dbo.Players", "League_LeagueID");
            AddForeignKey("dbo.Players", "League_LeagueID", "dbo.Leagues", "LeagueID");
        }
    }
}
