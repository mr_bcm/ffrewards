namespace FFRewards_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class onetomanyrelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlayerTeams", "Player_PlayerId", "dbo.Players");
            DropForeignKey("dbo.PlayerTeams", "Team_TeamId", "dbo.Teams");
            DropIndex("dbo.PlayerTeams", new[] { "Player_PlayerId" });
            DropIndex("dbo.PlayerTeams", new[] { "Team_TeamId" });
            CreateTable(
                "dbo.PlayertoTeams",
                c => new
                    {
                        PlayerId = c.Int(nullable: false),
                        TeamId = c.Int(nullable: false),
                        Starter = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.PlayerId, t.TeamId })
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
            AddColumn("dbo.Leagues", "Description", c => c.String());
            DropTable("dbo.PlayerTeams");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PlayerTeams",
                c => new
                    {
                        Player_PlayerId = c.Int(nullable: false),
                        Team_TeamId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Player_PlayerId, t.Team_TeamId });
            
            DropForeignKey("dbo.PlayertoTeams", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.PlayertoTeams", "PlayerId", "dbo.Players");
            DropIndex("dbo.PlayertoTeams", new[] { "TeamId" });
            DropIndex("dbo.PlayertoTeams", new[] { "PlayerId" });
            DropColumn("dbo.Leagues", "Description");
            DropTable("dbo.PlayertoTeams");
            CreateIndex("dbo.PlayerTeams", "Team_TeamId");
            CreateIndex("dbo.PlayerTeams", "Player_PlayerId");
            AddForeignKey("dbo.PlayerTeams", "Team_TeamId", "dbo.Teams", "TeamId", cascadeDelete: true);
            AddForeignKey("dbo.PlayerTeams", "Player_PlayerId", "dbo.Players", "PlayerId", cascadeDelete: true);
        }
    }
}
