namespace FFRewards_Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class databaseupdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Players", "Team_TeamId", "dbo.Teams");
            DropIndex("dbo.Players", new[] { "Team_TeamId" });
            CreateTable(
                "dbo.PlayerTeams",
                c => new
                    {
                        Player_PlayerId = c.Int(nullable: false),
                        Team_TeamId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Player_PlayerId, t.Team_TeamId })
                .ForeignKey("dbo.Players", t => t.Player_PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.Team_TeamId, cascadeDelete: true)
                .Index(t => t.Player_PlayerId)
                .Index(t => t.Team_TeamId);
            
            AddColumn("dbo.Leagues", "LeagueType", c => c.Int(nullable: false));
            AddColumn("dbo.Leagues", "ApplicationUser_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Rules", "RosterSize", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "Starters", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "Bench", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "QBStart", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "QBMax", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "RBStart", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "RBMax", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "FlexStart", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "WRStart", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "WRMax", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "TEStart", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "TEMax", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "DSTstart", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "DSTMax", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "PlaceKickStart", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "PlaceKickMax", c => c.Int(nullable: false));
            AddColumn("dbo.Rules", "PassingYards", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "InterceptionsThrown", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TDPass", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TwoPtPC", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "RushingYards", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TwoPtRC", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TDRush", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "ReceivingYards", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TwoPtRec", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TDReception", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "KickoffReturnTD", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FumbleRecoveryTD", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "InterceptionReturnTD", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "BlockedPuntFgTD", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "PuntReturnTD", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TotalFumblesLost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "PATMade", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "PATMissed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TotalFGMissed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FGForty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FGZero", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FGFifty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "Sack", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FumbleReturnTD", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "BlockedPuntPATFG", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FumbleRecovered", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "Interception", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "Safety", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "ZeroPointsAllowed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "OnePointAllowed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "SevenPointsAllowed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FourteenPointsAllowed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "TwentyEightPointsAllowed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "ThirtyFivePointsAllowed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "FourtySixPointsAllowed", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsSub100", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsSub200", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsSub300", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsSub400", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsSub450", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsSub500", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsSub550", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Rules", "YardsPlus550", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.AspNetUsers", "Wins", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Loses", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "League_LeagueID", c => c.Int());
            AddColumn("dbo.Teams", "Points", c => c.Double(nullable: false));
            CreateIndex("dbo.Leagues", "ApplicationUser_Id");
            CreateIndex("dbo.AspNetUsers", "League_LeagueID");
            AddForeignKey("dbo.Leagues", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUsers", "League_LeagueID", "dbo.Leagues", "LeagueID");
            DropColumn("dbo.Players", "Team_TeamId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Players", "Team_TeamId", c => c.Int());
            DropForeignKey("dbo.AspNetUsers", "League_LeagueID", "dbo.Leagues");
            DropForeignKey("dbo.PlayerTeams", "Team_TeamId", "dbo.Teams");
            DropForeignKey("dbo.PlayerTeams", "Player_PlayerId", "dbo.Players");
            DropForeignKey("dbo.Leagues", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.PlayerTeams", new[] { "Team_TeamId" });
            DropIndex("dbo.PlayerTeams", new[] { "Player_PlayerId" });
            DropIndex("dbo.AspNetUsers", new[] { "League_LeagueID" });
            DropIndex("dbo.Leagues", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Teams", "Points");
            DropColumn("dbo.AspNetUsers", "League_LeagueID");
            DropColumn("dbo.AspNetUsers", "Loses");
            DropColumn("dbo.AspNetUsers", "Wins");
            DropColumn("dbo.Rules", "YardsPlus550");
            DropColumn("dbo.Rules", "YardsSub550");
            DropColumn("dbo.Rules", "YardsSub500");
            DropColumn("dbo.Rules", "YardsSub450");
            DropColumn("dbo.Rules", "YardsSub400");
            DropColumn("dbo.Rules", "YardsSub300");
            DropColumn("dbo.Rules", "YardsSub200");
            DropColumn("dbo.Rules", "YardsSub100");
            DropColumn("dbo.Rules", "FourtySixPointsAllowed");
            DropColumn("dbo.Rules", "ThirtyFivePointsAllowed");
            DropColumn("dbo.Rules", "TwentyEightPointsAllowed");
            DropColumn("dbo.Rules", "FourteenPointsAllowed");
            DropColumn("dbo.Rules", "SevenPointsAllowed");
            DropColumn("dbo.Rules", "OnePointAllowed");
            DropColumn("dbo.Rules", "ZeroPointsAllowed");
            DropColumn("dbo.Rules", "Safety");
            DropColumn("dbo.Rules", "Interception");
            DropColumn("dbo.Rules", "FumbleRecovered");
            DropColumn("dbo.Rules", "BlockedPuntPATFG");
            DropColumn("dbo.Rules", "FumbleReturnTD");
            DropColumn("dbo.Rules", "Sack");
            DropColumn("dbo.Rules", "FGFifty");
            DropColumn("dbo.Rules", "FGZero");
            DropColumn("dbo.Rules", "FGForty");
            DropColumn("dbo.Rules", "TotalFGMissed");
            DropColumn("dbo.Rules", "PATMissed");
            DropColumn("dbo.Rules", "PATMade");
            DropColumn("dbo.Rules", "TotalFumblesLost");
            DropColumn("dbo.Rules", "PuntReturnTD");
            DropColumn("dbo.Rules", "BlockedPuntFgTD");
            DropColumn("dbo.Rules", "InterceptionReturnTD");
            DropColumn("dbo.Rules", "FumbleRecoveryTD");
            DropColumn("dbo.Rules", "KickoffReturnTD");
            DropColumn("dbo.Rules", "TDReception");
            DropColumn("dbo.Rules", "TwoPtRec");
            DropColumn("dbo.Rules", "ReceivingYards");
            DropColumn("dbo.Rules", "TDRush");
            DropColumn("dbo.Rules", "TwoPtRC");
            DropColumn("dbo.Rules", "RushingYards");
            DropColumn("dbo.Rules", "TwoPtPC");
            DropColumn("dbo.Rules", "TDPass");
            DropColumn("dbo.Rules", "InterceptionsThrown");
            DropColumn("dbo.Rules", "PassingYards");
            DropColumn("dbo.Rules", "PlaceKickMax");
            DropColumn("dbo.Rules", "PlaceKickStart");
            DropColumn("dbo.Rules", "DSTMax");
            DropColumn("dbo.Rules", "DSTstart");
            DropColumn("dbo.Rules", "TEMax");
            DropColumn("dbo.Rules", "TEStart");
            DropColumn("dbo.Rules", "WRMax");
            DropColumn("dbo.Rules", "WRStart");
            DropColumn("dbo.Rules", "FlexStart");
            DropColumn("dbo.Rules", "RBMax");
            DropColumn("dbo.Rules", "RBStart");
            DropColumn("dbo.Rules", "QBMax");
            DropColumn("dbo.Rules", "QBStart");
            DropColumn("dbo.Rules", "Bench");
            DropColumn("dbo.Rules", "Starters");
            DropColumn("dbo.Rules", "RosterSize");
            DropColumn("dbo.Leagues", "ApplicationUser_Id");
            DropColumn("dbo.Leagues", "LeagueType");
            DropTable("dbo.PlayerTeams");
            CreateIndex("dbo.Players", "Team_TeamId");
            AddForeignKey("dbo.Players", "Team_TeamId", "dbo.Teams", "TeamId");
        }
    }
}
