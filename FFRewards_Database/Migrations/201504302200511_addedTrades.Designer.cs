// <auto-generated />
namespace FFRewards_Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addedTrades : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addedTrades));
        
        string IMigrationMetadata.Id
        {
            get { return "201504302200511_addedTrades"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
