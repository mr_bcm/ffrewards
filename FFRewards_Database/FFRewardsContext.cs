﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;

namespace FFRewards_Database
{
    public class FFRewardsContext : IdentityDbContext<ApplicationUser>
    {
        public FFRewardsContext() : base("FFRewards", throwIfV1Schema: false) { }

        public static FFRewardsContext Create()
        {
            return new FFRewardsContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PlayertoTeam>().HasKey(x => new { x.PlayerId, x.TeamId });
            base.OnModelCreating(modelBuilder);
        }

        #region Database Tables

        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<Rules> Rule { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<PlayertoTeam> PlayertoTeams { get; set; }
        public DbSet<Trade> Trades { get; set; }

        #endregion

    }
}
