﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;
using FFRewards_Database.Interfaces;

namespace FFRewards_Database.Queries
{
    public class PlayerQueries : IPlayerQueries
    {
        private readonly FFRewardsContext _context;

        public PlayerQueries(FFRewardsContext context)
        {
            _context = context;
        }

        #region Get Players
        public Player GetPlayer(int playerId)
        {
            var player = _context.Players.SingleOrDefault(x => x.PlayerId == playerId);
            return player;
        }

        //public ICollection<Player> GetTeamPlayers(ApplicationUser user, int leagueId)
        //{
        //    var team = _context.Teams.Where(x => x.OwnerId == user.Id && x.LeagueId == leagueId).Select(x => x.Player);
        //    return team;
        //}

        public List<Player> GetPlayers()
        {
            var listPlayers = _context.Players.ToList();
            return listPlayers;
        }

        public List<Player> GetPlayers(Team team)
        {
            var listPlayers = _context.PlayertoTeams.Where(x => x.TeamId == team.TeamId).Select(x => x.Player).ToList();
            return listPlayers;
        }

        public List<Player> GetFreeAgents(League league)
        {
            var listFree = league.FreeAgents.ToList();
            return listFree;
        }
       
        

        //public List<Player> GetPlayers(Team team)
        //{
        //    var listPlayers = team.Players.ToList();
        //    return listPlayers;
        //}

        //public List<Player> GetPlayers(ApplicationUser user, int teamId)
        //{
        //    var listPlayers = user.Teams.SingleOrDefault(x => x.TeamId == teamId);
        //    return listPlayers.Players.ToList();
        //}


        #endregion
        public void AddPlayertoTeam(Team team, Player player)
        {
            PlayertoTeam pl = new PlayertoTeam();

            pl.Team = team;
            pl.Player = player;
            pl.Starter = false;

            _context.SaveChanges();
        }

        public void DeletePlayer(Team team, Player player)
        {
            var pl = _context.PlayertoTeams.SingleOrDefault(x => x.TeamId == team.TeamId && x.PlayerId == player.PlayerId);
            if (pl == null) return;
            _context.PlayertoTeams.Remove(pl);
            _context.SaveChanges();
        }

        public void UpdatePlayer(Team team, Player player)
        {
            var pl = _context.PlayertoTeams.SingleOrDefault(x => x.TeamId == team.TeamId && x.PlayerId == player.PlayerId);
            if (pl == null) return;
            pl.Player = player;
            _context.SaveChanges();
        }
    }
}
