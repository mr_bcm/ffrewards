﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;
using FFRewards_Database.Interfaces;

namespace FFRewards_Database.Queries
{
    public class RulesQueries : IRulesQueries
    {
        private readonly FFRewardsContext _context;

        public RulesQueries(FFRewardsContext context)
        {
            _context = context;
        }
        public Rules GetRules(int ruleId)
        {
            var rules = _context.Rule.SingleOrDefault(x => x.RulesID == ruleId);
            return rules;
        }

        public List<Rules> GetRulesList(ApplicationUser user)
        {
            var listRules = user.Rules.ToList();
            return listRules;
        }

        public void AddNewRule(ApplicationUser user, Rules rule)
        {
            if(user.Rules == null || !user.Rules.Any())
            {
                user.Rules = new Collection<Rules>();
            }

            user.Rules.Add(rule);
            _context.SaveChanges();
        }

        public void UpdateRules(ApplicationUser user, Rules rule)
        {
            var rules = user.Rules.SingleOrDefault(x => x.RulesID == rule.RulesID);

            if (rules == null) return;
            rules = rule;
            _context.SaveChanges();
        }

        public void DeleteRules(ApplicationUser user, Rules rule)
        {
            var rules = user.Rules.SingleOrDefault(x => x.RulesID == rule.RulesID);

            if (rule == null) return;
            _context.Rule.Remove(rule);
            _context.SaveChanges();
        }


    }
}
