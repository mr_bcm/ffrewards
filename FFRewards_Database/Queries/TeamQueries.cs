﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;
using FFRewards_Database.Interfaces;

namespace FFRewards_Database.Queries
{
    public class TeamQueries : ITeamQueries
    {
        private readonly FFRewardsContext _context;

        public TeamQueries(FFRewardsContext context)
        {
            _context = context;
        }

        public Team GetTeam(int teamId)
        {
            var team = _context.Teams.SingleOrDefault(x => x.TeamId == teamId);
            return team;
        }

        public Team GetTeam(ApplicationUser user, League league)
        {
            var team = user.Teams.SingleOrDefault(x => x.LeagueId == league.LeagueID);
            return team;
        }

        public List<Team> GetTeams(ApplicationUser user)
        {
            var listTeams = user.Teams.ToList();
            return listTeams;
        }

        public List<Team> GetTeams(int leagueId)
        {
            var listTeams = _context.Teams.Where(x => x.LeagueId == leagueId).ToList();
            return listTeams;

        }

        public void AddNewTeam(ApplicationUser user, Team team)
        {
            if (user.Teams == null || !user.Teams.Any())
            {
                user.Teams = new Collection<Team>();
            }

            user.Teams.Add(team);
            _context.SaveChanges();
        }

        public void UpdateTeam(ApplicationUser user, Team team)
        {
            var t = user.Teams.SingleOrDefault(x => x.TeamId == team.TeamId);

            if (t == null) return;
            t = team;
            _context.SaveChanges();
        }

        public void DeleteTeam(ApplicationUser user, Team team)
        {
            var t = user.Teams.SingleOrDefault(x => x.TeamId == team.TeamId);
            if (t == null) return;
            _context.Teams.Remove(team);
            _context.SaveChanges();
        }

    }
}