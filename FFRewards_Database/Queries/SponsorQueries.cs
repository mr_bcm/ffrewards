﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;
using FFRewards_Database.Interfaces;

namespace FFRewards_Database.Queries
{
    public class SponsorQueries : ISponsorQueries
    {
        private readonly FFRewardsContext _context;

        public SponsorQueries(FFRewardsContext context)
        {
            _context = context;
        }

        public Sponsor GetSponsor(int sponsorId)
        {
            var sponsor = _context.Sponsors.SingleOrDefault(x => x.SponsorID == sponsorId);
            return sponsor;
        }

        public List<Sponsor> GetSponsors()
        {
            var listSponsors = _context.Sponsors.ToList();
            return listSponsors;
        }

        public void AddNewSponsor(Sponsor sponsor)
        {
            if(!_context.Sponsors.Where(x => x.SponsorID == sponsor.SponsorID).Any())
            {
                _context.Sponsors.Add(sponsor);
                _context.SaveChanges();
            }
        }

        public void DeleteSponsor(Sponsor sponsor)
        {
            var spon = _context.Sponsors.SingleOrDefault(x => x.SponsorID == sponsor.SponsorID);
            if (spon == null) return;
            _context.Sponsors.Remove(sponsor);
            _context.SaveChanges();
        }

        public void UpdateSponsor(Sponsor sponsor)
        {
            var spon = _context.Sponsors.SingleOrDefault(x => x.SponsorID == sponsor.SponsorID);
            if (spon == null) return;
            spon = sponsor;
            _context.SaveChanges();
        }
    }
}
