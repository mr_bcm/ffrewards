﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;
using FFRewards_Database.Interfaces;

namespace FFRewards_Database.Queries
{
    public class LeagueQueries : ILeagueQueries
    {
        private readonly FFRewardsContext _context;

        public LeagueQueries(FFRewardsContext context)
        {
            _context = context;
        }

        #region Get Leagues

        /// <summary>
        /// SQL: select * from league where LeagueID = leagueId;
        /// </summary>
        /// <param name="leagueId"></param>
        /// <returns>
        /// The league with the specified id.
        /// </returns>
        public League GetLeague(int leagueId)
        {
            
            var league = _context.Leagues.SingleOrDefault(x => x.LeagueID == leagueId);

            return league;
        }

        /// <summary>
        /// SQL: select * from league;
        /// </summary>
        /// <returns>
        /// List of leagues available in the database
        /// </returns>
        public List<League> GetLeagues()
        {
            var leagueList = _context.Leagues.ToList();
            return leagueList;
        }

        public List<League> GetLeagues(int leagueType)
        {
            var leagueList = _context.Leagues.Where(x => x.LeagueType == leagueType).ToList();
            return leagueList;
        }

        public List<League> GetLeagues(ApplicationUser user)
        {
            var leagueList = user.Leagues.ToList();
            return leagueList;
        }

        public List<League> GetLeagues(Sponsor sponsor)
        {
            var leagueList = sponsor.Leagues.ToList();
            return leagueList;
        }

        #endregion

        public void AddNewLeague(ApplicationUser user, Sponsor sponsor, League league, Rules rule)
        {
            if (user.Leagues == null || !user.Leagues.Any())
            {
                user.Leagues = new Collection<League>();
            }

            league.SponsorId = sponsor.SponsorID;
            league.RulesId = rule.RulesID;
            league.Participants.Add(user);

            user.Leagues.Add(league);
            _context.SaveChanges();
        }

        public void DeleteLeague(ApplicationUser user, League league)
        {
            var leag = user.Leagues.SingleOrDefault(x => x.LeagueID == league.LeagueID);

            if (leag == null) return;
            _context.Leagues.Remove(league);
            _context.SaveChanges();
        }

        public void UpdateLeague(ApplicationUser user, League league)
        {
            var leag = user.Leagues.SingleOrDefault(x => x.LeagueID == league.LeagueID);

            if (leag == null) return;
            leag = league;
            _context.SaveChanges();
        }
    }
}
