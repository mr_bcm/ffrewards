﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFRewards_Database.Entities;
using FFRewards_Database.Interfaces;

namespace FFRewards_Database.Queries
{
    public class ParticipantQueries : IParticipantQueries
    {
        private readonly FFRewardsContext _context;

        public ParticipantQueries(FFRewardsContext context)
        {
            _context = context;
        }


        public List<ApplicationUser> GetParticipants(League league)
        {
            var listUser = league.Participants.ToList();
            return listUser;
        }

        public void AddParticipants(ApplicationUser user, League league) 
        {
            league.Participants.Add(user);
        }
    }
}
