﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class Sponsor
    {
        /// <summary>
        /// Sponsor id
        /// </summary>
        [Key]
        public int SponsorID { get; set; }

        /// <summary>
        /// Sponsor name
        /// </summary>
        [Display(Name = "Sponsor Name")]
        public string SponsorName { get; set; }

        /// <summary>
        /// Address of the sponsor
        /// </summary>
        [Display(Name = "Address")]
        public string Address { get; set; }


        /// <summary>
        /// Phone number of the sponsor
        /// </summary>
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [JsonIgnore]
        public Decimal Payout { get; set; }

        [Display(Name = "Logo URL")]
        public string LogoURL { get; set; }

        [Display(Name = "URL")]
        public string Url { get; set; }
        //create the logURL property
        //create the url property

        [Display(Name = "Leagues Sponsoring")]
        [JsonIgnore]
        public virtual ICollection<League> Leagues { get; set; }
    }
}
