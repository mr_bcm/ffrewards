﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class PlayertoTeam 
    {
        [Display(Name = "PlayerId")]
        public int PlayerId { get; set; }
        [JsonIgnore]
        public virtual Player Player { get; set; }

        [Display(Name = "TeamId")]
        public int TeamId { get; set; }
        [JsonIgnore]
        public virtual Team Team { get; set; }
        public bool Starter { get; set; }

        
        

    }
}
