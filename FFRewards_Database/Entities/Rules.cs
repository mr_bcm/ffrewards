﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class Rules
    {
        /// <summary>
        /// Rules id
        /// </summary>
        /// 
        [Key]
        public int RulesID { get; set; }


        /// <summary>
        /// Rule name
        /// </summary>
        [Display(Name = "Rule name")]
        public string RuleName { get; set; }

        [Display(Name = "Roster Size")]
        public int RosterSize { get; set; }

        [Display(Name = "Starters")]
        public int Starters { get; set; }

        [Display(Name = "Bench")]
        public int Bench { get; set; }

        [Display(Name = "QB Start")]
        public int QBStart { get; set; }

        [Display(Name = "QB Max")]
        public int QBMax { get; set; }

        [Display(Name = "RB Start")]
        public int RBStart { get; set; }

        [Display(Name = "RB Max")]
        public int RBMax { get; set; }

        [Display(Name = "Flex Start")]
        public int FlexStart { get; set; }

        [Display(Name = "WR Start")]
        public int WRStart { get; set; }

        [Display(Name = "WR Max")]
        public int WRMax { get; set; }

        [Display(Name = "TE Start")]
        public int TEStart { get; set; }

        [Display(Name = "TE Max")]
        public int TEMax { get; set; }

        //Team Defense/Special Teams
        [Display(Name = "DST Start")]
        public int DSTstart { get; set; }

        [Display(Name = "DST Max")]
        public int DSTMax { get; set; }

        [Display(Name = "Place-Kick Start")]
        public int PlaceKickStart { get; set; }

        [Display(Name = "Place-Kick Max")]
        public int PlaceKickMax { get; set; }

        [Display(Name = "Passing Yards")]
        public decimal PassingYards { get; set; }

        [Display(Name = "Interceptions Thrown")]
        public decimal InterceptionsThrown { get; set; }

        [Display(Name = "TD Pass")]
        public decimal TDPass { get; set; }

        [Display(Name = "Two Point Passing Conversion")]
        public decimal TwoPtPC { get; set; }

        [Display(Name = "Rushing Yards")]
        public decimal RushingYards { get; set; }
        
        [Display(Name = "Two Point Rushing Conversion")]
        public decimal TwoPtRC { get; set; }

        [Display(Name = "TD Rush")]
        public decimal TDRush { get; set; }

        [Display(Name = "Receiving Yards")]
        public decimal ReceivingYards { get; set; }

        [Display(Name = "Two Point Receiving Conversion")]
        public decimal TwoPtRec { get; set; }

        [Display(Name = "TD Reception")]
        public decimal TDReception { get; set; }

        [Display(Name = "Kickoff Return TD")]
        public decimal KickoffReturnTD { get; set; }

        [Display(Name = "Fumble Recovery TD")]
        public decimal FumbleRecoveryTD { get; set; }

        [Display(Name = "Interception Return TD")]
        public decimal InterceptionReturnTD { get; set; }

        //Blocked Punt or FG return for TD
        [Display(Name = "Blocked Punt FG TD")]
        public decimal BlockedPuntFgTD { get; set; }

        [Display(Name = "Punt Return TD")]
        public decimal PuntReturnTD { get; set; }

        [Display(Name = "Total Fumbles Lost")]
        public decimal TotalFumblesLost { get; set; }

        [Display(Name = "PAT Made")]
        public decimal PATMade { get; set; }

        [Display(Name = "PAT Missed")]
        public decimal PATMissed { get; set; }

        [Display(Name = "Total FG Missed")]
        public decimal TotalFGMissed { get; set; }

        [Display(Name = "40-49yd FG")]
        public decimal FGForty { get; set; }

        [Display(Name = "0-39yd FG")]
        public decimal FGZero { get; set; }
        
        [Display(Name = "50+yd FG")]
        public decimal FGFifty { get; set; }

        [Display(Name = "Sack")]
        public decimal Sack { get; set; }

        [Display(Name = "Fumble Return TD")]
        public decimal FumbleReturnTD { get; set; }

        [Display(Name = "Blocked Punt PAT FG")]
        public decimal BlockedPuntPATFG { get; set; }

        [Display(Name = "Fumble Recovered")]
        public decimal FumbleRecovered { get; set; }

        [Display(Name = "Blocked Punt FG Return TD")]
        public decimal BlockedPuntFGTD { get; set; }

        [Display(Name = "Interception")]
        public decimal Interception { get; set; }

        [Display(Name = "Safety")]
        public decimal Safety { get; set; }

        [Display(Name = "Zero Points Allowed")]
        public decimal ZeroPointsAllowed { get; set; }

        [Display(Name = "1-6 Points Allowed")]
        public decimal OnePointAllowed { get; set; }

        [Display(Name = "7-13 Points Allowed")]
        public decimal SevenPointsAllowed { get; set; }

        [Display(Name = "14-17 Points Allowed")]
        public decimal FourteenPointsAllowed { get; set; }

        [Display(Name = "28-34 Points Allowed")]
        public decimal TwentyEightPointsAllowed { get; set; }
        
        [Display(Name = "35-45 Points Allowed")]
        public decimal ThirtyFivePointsAllowed { get; set; }
        
        [Display(Name = "46+ Points Allowed")]
        public decimal FourtySixPointsAllowed { get; set; }

        [Display(Name = "Under 100 Yards Allowed")]
        public decimal YardsSub100 { get; set; }

        [Display(Name = "100-199 Yards Allowed")]
        public decimal YardsSub200 { get; set; }

        [Display(Name = "200-299 Yards Allowed")]
        public decimal YardsSub300 { get; set; }

        [Display(Name = "350-399 Yards Allowed")]
        public decimal YardsSub400 { get; set; }

        [Display(Name = "400-449 Yards Allowed")]
        public decimal YardsSub450 { get; set; }

        [Display(Name = "450-499 Yards Allowed")]
        public decimal YardsSub500 { get; set; }

        [Display(Name = "500-549 Yards Allowed")]
        public decimal YardsSub550 { get; set; }

        [Display(Name = "550+ Yards Allowed")]
        public decimal YardsPlus550 { get; set; }
              
                                        
        /// <summary>
        /// user id of the owner of rules
        /// </summary>
        [ForeignKey("User")]
        public string OwnerID { get; set; }
        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }

    }
}
