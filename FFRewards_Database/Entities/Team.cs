﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class Team
    {
        [Key]
        public int TeamId { get; set; }

        [ForeignKey("User")]
        public string OwnerId { get; set; }
        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }

        [ForeignKey("League")]
        public int LeagueId { get; set; }
        [JsonIgnore]
        public virtual League League { get; set; }

        [Display(Name = "Team Name")]
        public string TeamName { get; set; }

        [Display(Name = "Points")]
        public double Points { get; set; }

        [Display(Name = "Icon")]
        public string Icon { get; set; }

        [Display(Name = "Player in Team")]
        [JsonIgnore]
        public virtual ICollection<PlayertoTeam> PlayertoTeam { get; set; }
    }
}
