﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class Trade
    {
        [Key]
        public int TradeId { get; set; }

        [ForeignKey("Player")]
        public int PlayerId { get; set; }
        [JsonIgnore]
        public virtual Player Player { get; set; }

        [ForeignKey("STeam")]
        public int STeamId { get; set; }
        [JsonIgnore]
        public virtual Team STeam { get; set; }

        [ForeignKey("DTeam")]
        public int DTeamId { get; set; }
        [JsonIgnore]
        public virtual Team DTeam { get; set; }
    }
}
