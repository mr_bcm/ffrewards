﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class League
    {
        [Key]
        public int LeagueID { get; set; }

        [ForeignKey("User")]
        public string OwnerId { get; set; }
        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }

        [ForeignKey("Rules")]
        public int RulesId { get; set; }
        [JsonIgnore]
        public virtual Rules Rules { get; set; }

        [ForeignKey("Sponsor")]
        public int SponsorId { get; set; }
        [JsonIgnore]
        public virtual Sponsor Sponsor { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "League Name")]
        public string LeagueName { get; set; }

        [Display(Name = "Prize")]
        public Decimal Prize { get; set; }

        /// <summary>
        /// League Type:
        /// 0: Sponsored
        /// 1: Weekly
        /// 2: Custom
        /// </summary>
        [Display(Name = "League Type")]
        public int LeagueType { get; set; }

        [Display(Name = "Free Agents")]
        [JsonIgnore]
        public virtual ICollection<Player> FreeAgents { get; set; }
    
        [Display(Name = "Participants")]
        [JsonIgnore]
        public virtual ICollection<ApplicationUser> Participants { get; set; }
    }
}
