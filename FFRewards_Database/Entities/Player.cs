﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class Player
    {
        [Key]
        public int PlayerId { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Team Name")]
        public string TeamName { get; set; }

        [JsonIgnore]
        public bool Injured { get; set; }

        [Display(Name = "Position")]
        public string Position { get; set; }

        [JsonIgnore]
        public bool Active { get; set; }

        [Display(Name = "Pass Completed")]
        public string PassCompleted { get; set; }

        [Display(Name = "Pass Attempted")]
        public string PassAttempted { get; set; }

        [Display(Name = "Pass Yards")]
        public string PassYards { get; set; }

        [Display(Name = "Pass Touchdowns")]
        public string PassTouchdown { get; set; }

        [Display(Name = "Pass Interception")]
        public string PassInterception { get; set; }

        [Display(Name = "Rush Attempts")]
        public string RushAttempts { get; set; }

        [Display(Name = "Rush Yards")]
        public string RushYards { get; set; }

        [Display(Name = "Rush Touchdowns")]
        public string RushTouchdowns { get; set; }

        [Display(Name = "Receive Success")]
        public string ReceiveSuccess { get; set; }

        [Display(Name = "Receive Yards")]
        public string ReceiveYards { get; set; }

        [Display(Name = "Receive Touchdowns")]
        public string ReceiveTouchdowns { get; set; }

        [Display(Name = "Receive Target")]
        public string ReceiveTarget { get; set; }

        [Display(Name = "Total 2 Point Conversion")]
        public string Total2PointCon { get; set; }

        [Display(Name = "Total Fumble Lost")]
        public string TotalFumbleLost { get; set; }

        [Display(Name = "Total Return Touchdown")]
        public string TotalRetTouchdown { get; set; }

        [Display(Name = "Teams a part of")]
        [JsonIgnore]
        public virtual ICollection<PlayertoTeam> PlayertoTeam { get; set; }

        [Display(Name = "Leagues a part of")]
        [JsonIgnore]
        public virtual ICollection<League> Leagues { get; set; }
    }
}
