﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FFRewards_Database.Entities
{
    public class ApplicationUser :IdentityUser
    {

        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }

        [JsonIgnore]
        public bool Admin { get; set; }

        [Display(Name = "Teams Owned")]
        [JsonIgnore]
        public virtual ICollection<Team> Teams { get; set; }

        [Display(Name = "Leagues a Part of")]
        [JsonIgnore]
        public virtual ICollection<League> Leagues { get; set; }

        [Display(Name = "Profile Pic URL")]
        public string ImageUrl { get; set; }

        [Display(Name = "Rules Created")]
        [JsonIgnore]
        public virtual ICollection<Rules> Rules { get; set; }

        [Display(Name = "Wins")]
        public int Wins { get; set; }
        [Display(Name = "Loses")]
        public int Loses { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

    }
}
