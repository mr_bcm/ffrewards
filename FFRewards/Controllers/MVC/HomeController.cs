﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FFRewards.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult Leagues()
        {
            ViewBag.Title = "Leagues";

            return View();
        }

        public ActionResult MyTeams()
        {
            ViewBag.Title = "My Teams";

            return View();
        }

        public ActionResult Profile()
        {
            ViewBag.Title = "Profile";

            return View();
        }

        public ActionResult Draft()
        {
            ViewBag.Title = "Draft";

            return View();
        }

        public ActionResult RegisterBinding()
        {
            FFRewards.Models.RegisterBindingModel model = new FFRewards.Models.RegisterBindingModel();
            return View(model);
        }
    }
}
