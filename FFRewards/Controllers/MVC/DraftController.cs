﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FFRewards.Controllers
{
    public class DraftController : Controller
    {
       
        public ActionResult Draft()
        {
            ViewBag.Title = "Draft";

            return View();
        }

        public ActionResult RegisterBinding()
        {
            FFRewards.Models.RegisterBindingModel model = new FFRewards.Models.RegisterBindingModel();
            return View(model);
        }
    }
}
