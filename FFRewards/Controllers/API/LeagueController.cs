﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FFRewards.Models;
using FFRewards_Database;
using FFRewards_Database.Entities;
using FFRewards_Database.Queries;
using FFRewards_Database.Interfaces;
using Microsoft.AspNet.Identity;

namespace FFRewards.Controllers.API
{
    [Authorize]
    [RoutePrefix("api/League")]
    public class LeagueController : ApiController
    {

        private readonly FFRewardsContext _context;
        private readonly IPlayerQueries _PlayerQueries;
        private readonly ILeagueQueries _LeagueQueries;
        private readonly ITeamQueries _TeamQueries;
        private readonly IRulesQueries _RulesQueries;
        private readonly IParticipantQueries _ParticipantQueries;

        public LeagueController()
        {
            _context = new FFRewardsContext();
            _PlayerQueries = new PlayerQueries(_context);
            _LeagueQueries = new LeagueQueries(_context);
            _TeamQueries = new TeamQueries(_context);
            _RulesQueries = new RulesQueries(_context);
            _ParticipantQueries = new ParticipantQueries(_context);
        }

        //Queries have not been written yet, when they are added this will be uncommented and finished
        [AllowAnonymous]
        [Route("JoinLeague")]
        public IHttpActionResult JoinLeague(ApplicationUser user, League league)
        {
            if (_ParticipantQueries.GetParticipants(league).ToList().Count == 10)   // count should work, but double check later
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "League is full!")); // league is full so nothing happens and we leave
            _ParticipantQueries.AddParticipants(user, league);    // add the user to the league
            return Ok();
        }

        [AllowAnonymous]
        [Route("PopulateTeam")]
        public IHttpActionResult PopulateTeam()
        {
            string pass = "";
            League league = _LeagueQueries.GetLeague(1); //currently hard-coding our test league
            //string name = System.Web.Security.Membership.GetUser().;
            ApplicationUser user = _context.Users.Single(x => x.UserName == "ahaynes15@my.whitworth.edu");
            //user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);

            //string id = User.Identity.GetUserId();
            //ApplicationUser user = _context.Users.FirstOrDefault(x => x.Id == id);
            Team team = _TeamQueries.GetTeam(user, league);
            List<Player> players = _PlayerQueries.GetPlayers(team);
            for (int i = 0; i < players.Count(); i++)
            {
                pass = pass + " <div class=\"draft-player\" data-current-player-id=\"" + players[i].PlayerId + "\" data-current-player-name=\"" + players[i].FirstName + " " + players[i].LastName + "\">"
                   + "<div class=\"thirty fleft\">"
                   + "<li class=\"team-feed-item\">" + players[i].FirstName + " " + players[i].LastName + ", " + players[i].Position + "</li>"
                          + "</div>"
                          + "<div class=\"clr\"></div>"
                       + "</div>";
            }
            return Ok<string>(pass);
        }

        // Added by Tim
        [AllowAnonymous]
        [Route("PopulateTeamFreeAgents")]
        public IHttpActionResult PopulateTeamFreeAgents()
        {
            string pass = "";
            League league = _LeagueQueries.GetLeague(1); //currently hard-coding our test league
            List<Player> players = _PlayerQueries.GetFreeAgents(league);
            for (int i = 0; i < players.Count(); i++)
            {
                pass = pass + "<li class=\"team-feed-item\"> " + players[i].FirstName + " " + players[i].LastName + "</li>";
            }
            return Ok<string>(pass);
        }


        [AllowAnonymous]
        [Route("PopulateDraft")]
        public IHttpActionResult PopulateDraft()
        {
            //string pass = "";
            League league = _LeagueQueries.GetLeague(1); //currently hard-coding our test league
            List<Player> players = _PlayerQueries.GetFreeAgents(league);
            
            return Json(players);
        }


        // Take in applicationuser and league, check if the user you are checking in exists in the list, if so continue
        // actually, can just pass in team and league (application user is represented by their team, so we don't need it)
        [AllowAnonymous]
        [Route("Draft")]
        public IHttpActionResult Draft(RosterDraftBindingModel model)
        {
            // don't think we need these anymore
            //Team team = new Team();
            //League league = new League();

            var user = _context.Users.Single(x => x.UserName == User.Identity.Name);    // probably have to change when binding model is updated with user token
            var players = _PlayerQueries.GetPlayers();
            var teams = _TeamQueries.GetTeams(model.TeamID);
            var rules = _RulesQueries.GetRules(1);  // 1 is the ID for the default ruleset (only ruleset ...)
            var name = User.Identity.Name;
            int counter = 0; //maybe -1, keeps track of who's turn it is
            int counter2 = 0;   // Keeps track of players that have been drafted
            Team userTeam;
            Player curPlayer;
            bool toggle = true;


            if (user.Id == teams[counter].OwnerId)  // If true, user gets to pick a player
            {
                userTeam = teams[counter];
                if (counter2 == (teams.Count * rules.RosterSize))
                {
                    return Ok();    // draft successfully completes (need to indicate that draft is over, return notOk())
                }
                else
                    counter2++;
                if (toggle == true)
                {
                    counter++;
                }
                else
                {
                    counter--;
                }
                if (counter == -1 || counter == teams.Count)
                {
                    toggle = !toggle;
                }
                //TODO, logic for when time runs out or player has not picked. They will be auto given a player to their team


                //assignment logic
                curPlayer = _PlayerQueries.GetPlayer(model.PlayerID);
                _PlayerQueries.AddPlayertoTeam(userTeam, curPlayer);


            }
            else
            {
                return Ok();
            }


            return Ok();

        }
    }
}