﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace FFRewards.Models
{
    public class RosterDraftBindingModel
    {
        [Required]
        [Display(Name = "User ID")]     // this will later change to the user based on the token that we give them when they login
        public string UserID { get; set; }

        [Required]
        [Display(Name = "League ID")]
        public int LeagueID { get; set; }

        [Required]
        [Display(Name = "Team ID")]     // when we get user working we can remove this and find it based off of the League ID
        public int TeamID { get; set; }

        [Required]
        [Display(Name = "Player ID")]
        public int PlayerID { get; set; }
    }

}